/**
 *  profile repository 
 *
 * @name SkopR
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2018 Contus. All rights reserved.
 */
rec = require('../../database/models/').recruiters;
module.exports = {
    /**
     * get profile data
     * @param {*} req 
     * @param {*} res 
     * @param {*} callback 
     */
    profileObjDataRep(req, res, callback) {
        rec.findAll({
                where: {
                    recruiter_email_id: req.params.key
                },
            })
            .then(function (cf) {
                return callback(cf, 'success');
            })
            .catch(function (error) {
                return callback(error, 'error');

            });
    },

    profileEditObjDataRep(req, res, callback) {
        rec.update(req.body, {
                where: {
                    recruiter_email_id: req.params.key
                }
            })
            .then(function (updatedRecords) {
                return callback('Updated', 'success');
            })
            .catch(function (error) {
                return callback(error, 'error');
            });

    }


};