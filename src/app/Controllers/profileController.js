/**
 *  profile controller 
 *
 * Bridge between response and request 
 * 
 * @name SkopR
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2018 Contus. All rights reserved.
 */

/**
 * Call candidate repository module and response formater service
 */
profileObj = require('../Repositories/profileRepositories');
responseFormatter = require('../../services/responseFormatter');
var config = module.exports = {
    /**
     * This method using to call the education delete method
     * 
     * @param {*} req 
     * @param {*} res 
     * @return {json} row 
     */


    profileData(req, res) {
        profileObj.profileObjDataRep(req, res, function (row, type) {
            config.responseObj(row, res, type);
        });
    },

    profileEditData(req, res) {
        profileObj.profileEditObjDataRep(req, res, function (row, type) {
            config.responseObj(row, res, type);
        });
    },
    /**
     * This method using to call the response formater menthod
     * Response formater method used to build stand formate of response like error code and header...
     * @param {*} req 
     * @param {*} res 
     * @return {json} obj
     */

    responseObj: function (row, res, type) {
        responseFormatter[type](row, res, function (obj, statusCode) {
            res.status(statusCode).json(obj);
        });
    }
};