var express = require('express');
var router = express.Router();
profileCtl = require('../app/Controllers/profileController');
router.get('/api/v1/profile/:key',function(req,res){
    profileCtl.profileData(req,res);
})
router.put('/api/v1/profile/edit/:key',function(req,res){
    profileCtl.profileEditData(req,res);
})

module.exports = router;
