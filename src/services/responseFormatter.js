var item={};
module.exports= {
    success:function(data,res,callback){
            item={
                'statusCode':200,
                'data':data,
                 'error':{
                     'status':false
                 }
    }
           return callback(item,200); 
    },
    error:function(data,res,callback){
        var statusCode=500;
        var errorMessage='The server is busy now.Try again later';
        if(data.errors && data.errors[0] && data.errors[0].message){
           statusCode=422;
           errorMessage=data.errors[0].message;
        }
        item={
            'statusCode':statusCode,
             'error':{
                 'status':true,
                 'message':{
                    'server':errorMessage,        
                }  
             }
        }
        return callback(item,statusCode); 
    }
}