'use strict';
module.exports = (sequelize, DataTypes) => {
  var recruiters = sequelize.define("recruiters", {
    recruiter_email_id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    recruiter_name: DataTypes.STRING,
    recruiter_mobile: DataTypes.STRING,
    linkedin_profile: DataTypes.STRING,
    employer_id: DataTypes.INTEGER,
    employer_name: DataTypes.STRING,
    title: DataTypes.STRING,
    location: DataTypes.STRING,
    password_hash_b64: DataTypes.STRING,
    password_salt_b64: DataTypes.STRING,
    budget_limit_remaining: DataTypes.FLOAT,
    office_phone: DataTypes.STRING,
    recruiter_type: DataTypes.STRING,
  }, {
    underscored: true,
    classMethods: {
      associate: function (models) {
        //
      }
    }
  });
  return recruiters;
};