require('dotenv').config({path: '../../.env'});
module.exports = {
	development: {
		username:process.env.USER_NAME,
		password:process.env.PASSWORD,
		database:process.env.DATABASE_NAME,
		host:process.env.HOST,
		dialect:process.env.DIALECT,
		CONFIG_DATABASE_NAME:process.env.CONFIG_DATABASE_NAME
  },
	
};
