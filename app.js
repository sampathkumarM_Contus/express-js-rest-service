var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var index = require('./src/routes/api');
var app = express();
var cors = require('cors');
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/',index); 
app.use('/profile/:key',index); 
app.use('/profile/edit/:key',index); 
// serve static assets normally (eventually - dont seem to work now)
app.use(express.static(__dirname + '/static'))
app.use(express.static(__dirname + '/images'))
const baseDir = __dirname;
console.log('base directory is ' + baseDir)
// handle every other route with index.html, which will contain
// a script tag to your application's JavaScript file(s).
app.use('*', function (request, response) {

  let requestPath = request.originalUrl;
  console.log('request=' + requestPath);

  if (requestPath.endsWith('.css') ||
    requestPath.endsWith('.js') ||
    requestPath.endsWith('.ico') ||
    requestPath.match(/\/static\/.*$/) ||
    requestPath.match(/\/images\/.*$/)) {
    console.log('sending=' + baseDir + requestPath);
    response.sendFile(baseDir + requestPath);
  } else {
    console.log('sending=' + baseDir + '/index.html');
    response.sendFile(baseDir + '/index.html');
  }
})
module.exports = app;